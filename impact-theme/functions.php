<?php 

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

function wpt_excerpt_length( $length ) {
	return 16;
}
add_filter( 'excerpt_length', 'wpt_excerpt_length', 999 );

function register_theme_menus() {

	register_nav_menus(
		array(
			'primary-menu' 	=> __( 'Primary Menu', 'treehouse-portfolio' )			
		)
	);

}
add_action( 'init', 'register_theme_menus' );


function wpt_create_widget( $name, $id, $description ) {

	register_sidebar(array(
		'name' => __( $name ),	 
		'id' => $id, 
		'description' => __( $description ),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="module-heading">',
		'after_title' => '</h2>'
	));

}

wpt_create_widget( 'Page Sidebar', 'page', 'Displays on the side of pages with a sidebar' );
wpt_create_widget( 'Blog Sidebar', 'blog', 'Displays on the side of pages in the blog section' );


function wpt_theme_styles() {

	wp_enqueue_style( 'gfontLato_css', 'http://fonts.googleapis.com/css?family=Lato:400,700' );
	wp_enqueue_style( 'gfontMaterializeIcon_css', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
	wp_enqueue_style( 'materialize_css', get_template_directory_uri() . '/assets/css/materialize.css' );
	wp_enqueue_style( 'custom_css', get_template_directory_uri() . '/assets/css/main.css' );
	wp_enqueue_style( 'style_css', get_template_directory_uri() . '/style.css' );

}
add_action( 'wp_enqueue_scripts', 'wpt_theme_styles' );

function wpt_theme_js() {

	wp_enqueue_script( 'jquery_js', get_template_directory_uri() . '/assets/js/jquery-2.1.4.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'materialize_js', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/js/materialize.min.js', array('jquery', 'jquery_js'), '', true );		
	wp_enqueue_script( 'main_js', get_template_directory_uri() . '/assets/js/functions.js', array('jquery', 'jquery_js'), '', true );		

}
add_action( 'wp_enqueue_scripts', 'wpt_theme_js' );








?>