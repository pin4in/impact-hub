<!DOCTYPE html>
<html lang="ru">
  <head>
    <title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo bloginfo('template_url'); ?>/assets/img/favicon.png">
    <meta property="og:site_name" content="Impact Academy" />
    <meta property="og:title" content="Академия социальных инноваций - Impact Academy" />  
    <meta property="og:description" content="Здесь учат создавать бизнес, который меняет мир" />  
    <meta property="og:type" content="website" />  
    <meta property="og:image" content="<?php echo bloginfo('template_url'); ?>/assets/img/fb.jpg" />  
     <?php wp_head(); ?>
  </head>
   <body>

<!-- ***************** -->
<!--       HEADER      -->
<!-- ***************** -->

    <div class="navbar-fixed">
      <nav class="header">
        <div class="nav-wrapper container">
          <a href=" <?php bloginfo('url'); ?> " class="brand-logo"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/hub-logo.png" alt=""></a>
          <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
          <ul class="right hide-on-med-and-down">
            <li><a href="http://www.impactacademy.com.ua/#who">Для кого?</a></li>
            <li><a href="http://www.impactacademy.com.ua/#speakers">Преподаватели</a></li>
            <li><a href="http://www.impactacademy.com.ua/#program">Программа</a></li>
            <li><a href="http://www.impactacademy.com.ua/#prices">Стоимость</a></li>
            <li><a href="http://www.impactacademy.com.ua/#reviews">Отзывы</a></li>
            <li><a href="http://www.impactacademy.com.ua/#contacts">Контакты</a></li>
            <li class="header__registration"><a href="http://www.impactacademy.com.ua/registration/">Регистрация</a></li>
          </ul>
          <ul class="side-nav" id="mobile-demo">
            <li><a href="http://www.impactacademy.com.ua/#who">Для кого?</a></li>
            <li><a href="http://www.impactacademy.com.ua/#speakers">Преподаватели</a></li>
            <li><a href="http://www.impactacademy.com.ua/#program">Программа</a></li>
            <li><a href="http://www.impactacademy.com.ua/#prices">Стоимость</a></li>
            <li><a href="http://www.impactacademy.com.ua/#reviews">Отзывы</a></li>
            <li><a href="http://www.impactacademy.com.ua/#contacts">Контакты</a></li>
            <li class="header__registration"><a href="http://www.impactacademy.com.ua/registration/">Регистрация</a></li>
          </ul>
        </div>
      </nav>
    </div>