<?php get_header(); ?>

    <?php get_template_part('/partials/section','hero'); ?>
    <?php get_template_part('/partials/section','bonuses'); ?>
    <?php get_template_part('/partials/section','who'); ?>
    <?php get_template_part('/partials/section','speakers'); ?>
    <?php get_template_part('/partials/section','program'); ?>
    <?php get_template_part('/partials/section','prices'); ?>
    <?php get_template_part('/partials/section','reviews'); ?>
    <?php get_template_part('/partials/section','subscribe'); ?>
    <?php get_template_part('/partials/section','contacts'); ?>
    <?php get_template_part('/partials/section','map'); ?>
  
<?php get_footer(); ?>