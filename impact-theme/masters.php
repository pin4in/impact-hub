<div id="masters" class="parallax-container center reward-section"> 
      <!--banner content-->
      <div class="container">
        <h2>Наставники</h2>
      </div>
      <!--parallax img-->
      <div class="parallax"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/teachers.jpg" alt="программа наставничества"></div>
    </div>
    <div class="row bg-white padding-top padding-bottom">
      <div class="container"> 
        <div class="jcarousel-wrapper">
          <div class="jcarousel">
            <ul>
            
            <?php 
            // load masters data \\

            $query = new WP_Query( 'category_name=masters' );

            if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post(); ?>
              <li><?php the_post_thumbnail(); ?>
                <p class="upper-case big no-margin-bottom"><?php the_title(); ?></p>
                <p class="upper-case light"><?php the_field('specialization'); ?></p>
                <p class="light"><?php the_content(); ?></p>
              </li>
             <?php endwhile; endif; wp_reset_postdata(); ?>
 
           
            </ul>
          </div><a href="#" class="jcarousel-control-prev"><i class="material-icons">keyboard_arrow_left</i></a><a href="#" class="jcarousel-control-next"><i class="material-icons">keyboard_arrow_right  </i></a>
        </div>
      </div>
    </div>