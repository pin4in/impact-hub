<!-- ***************** -->
<!--       BONUSES     -->
<!-- ***************** -->
    <div class="bonuses container">
      <p class="bonuses__intro">образовательная программа о том,  как превращать социальные проблемы в бизнес-решения, И создавать на основе этого успешные компании и проекты.</p>
      <div class="row">
        <div class="col l4 m6 s12 bonuses__item">
          <div class="bonuses__item__num">54</div>
          <h3 class="bonuses__item__title">Часа <br>бизнес-теории</h3>
          <p class="bonuses__item__text">от ведущих украинских практиков, экспертов, ТОП-спикеров и предпринимателей.</p>
        </div>        
        <div class="col l4 m6 s12 bonuses__item">
          <div class="bonuses__item__num">36</div>
          <h3 class="bonuses__item__title">Часов <br> проектной работы</h3>
          <p class="bonuses__item__text">в командах над практическими задачами и бизнес-кейсами.</p>
        </div>
        <div class="col l4 m12 s12 bonuses__item">
          <div class="bonuses__item__num">30</div>
          <h3 class="bonuses__item__title">часов <br>консультаций</h3>
          <p class="bonuses__item__text">с экспертами различных отраслей для наиболее эффективной поддержки проектов и их реализации.</p>
        </div>
      </div>
    </div>