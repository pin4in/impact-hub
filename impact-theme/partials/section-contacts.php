<!-- ***************** -->
<!--      Contacts     -->
<!-- ***************** -->
    <span id="contacts" class="anchor_nav"></span>
    <div class="container contacts">
      <h2>Свяжитесь с нами</h2>
      <p>Оставьте ваши контакты и мы с радостью ответим на все вопросы.</p>

      <?php echo do_shortcode( '[contact-form-7 id="26" title="Форма обратной связи"]' );?>
  
    </div>  