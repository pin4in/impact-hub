<!-- ***************** -->
<!--        HERO       -->
<!-- ***************** -->
    <div class="hero parallax-container">
      <div class="container">
        <h1>Академия социальных инноваций</h1>
        <p>Здесь учат создавать бизнес, который меняет мир</p>
      </div>
      <div class="parallax"><img class="parallax-img" src="<?php echo bloginfo('template_url'); ?>/assets/img/hero.jpg"></div>
    </div>