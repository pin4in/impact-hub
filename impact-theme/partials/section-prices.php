<!-- ***************** -->
<!--      PRICES       -->
<!-- ***************** -->
    <span id="prices" class="anchor_nav"></span>
    <div class="prices">
      <div class="container">
        <h2>Стоимость участия</h2>
        <div class="row">
          <div class="col l4 s12">
            <div class="prices__item">
              <div class="prices__item__title">при оплате до 20.08</div>
              <div class="prices__item__price">
                <p>«EARLY BIRD»</p>
                8 500 грн
              </div>
              <a href="http://www.impactacademy.com.ua/registration/" class="waves-effect waves-light btn prices__item__button">регистрация</a>
            </div>
          </div>
          <div class="col l4 s12">
            <div class="prices__item prices__item--disabled">
              <div class="prices__item__title">при оплате до 10.09</div>
              <div class="prices__item__price">
                <p>«basic»</p>
                10 000 грн
              </div>
              <a class="waves-effect waves-light btn prices__item__button disabled">регистрация</a>
            </div>
          </div>
          <div class="col l4 s12">
            <div class="prices__item prices__item--disabled">
              <div class="prices__item__title">при оплате после 10.09</div>
              <div class="prices__item__price">
                <p>«Last Call»</p>
                12 000 грн
              </div>
              <a class="waves-effect waves-light btn prices__item__button disabled">регистрация</a>
            </div>
          </div>
        </div>
      </div>
    </div>