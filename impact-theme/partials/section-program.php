<!-- ***************** -->
<!--      PROGRAM      -->
<!-- ***************** -->
    <span id="program" class="anchor_nav"></span>
    <div class="program container">
      <h2>программа</h2>
      <div class="row">
        <div class="col l4 m6 s12 program__item">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/social.png" alt="">
          <h3 class="program__item__title">Модуль 1. Социальное <br> предпринимательство</h3>
          <p class="program__item__text">Определение понятия, глобальные <br>тенденции, лучшие мировые и <br> украинские практики.</p>
        </div>
        <div class="col l4 m6 s12 program__item program__item--mid">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/ideas.png" alt="">
          <h3 class="program__item__title">Модуль 2. Поиск <br>бизнес-идей</h3>
          <p class="program__item__text">Обзор рыночных трендов, оценка потенциала <br> идеи, создание индивидуальной <br>t tбизнес-модели. </p>
        </div>
        <div class="col l4 m6 s12 program__item program__item--ri">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/marketing.png" alt="">
          <h3 class="program__item__title">Модуль 3. Маркетинг</h3>
          <p class="program__item__text">Основы маркетинга, определение целевой <br> аудитории, создание коммуникационной <br> стратегии.
</p>
        </div>
        <div class="col l4 m6 s12 program__item">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/finance.png" alt="">
          <h3 class="program__item__title">Модуль 4. Финансы и <br> правовые аспекты</h3>
          <p class="program__item__text">Бюджетирование, финансовое <br> планирование, учет и правовые основы <br> организации бизнеса.</p>
        </div>
        <div class="col l4 m6 s12 program__item program__item--mid">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/salse.png" alt="">
          <h3 class="program__item__title">Модуль 5. Продажи и <br> бизнес-коммуникации<br> 
предпринимательство</h3>
          <p class="program__item__text">Внутренние и внешние коммуникации,<br>  стратегия, тактика и техники продаж.</p>
        </div>
        <div class="col l4 m6 s12 program__item program__item--ri">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/teamwork.png" alt="">
          <h3 class="program__item__title">Модуль 6. Работа <br> с командой</h3>
          <p class="program__item__text">Навыки руководителя: создание команды, <br> подбор и оценка кандидатов, управление <br> командой.
</p>
        </div>
        <div class="col l4 m6 s12 program__item--last program__item">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/plan.png" alt="">
          <h3 class="program__item__title">Модуль 7. План <br>и реализация</h3>
          <p class="program__item__text">Стратегическое планирование и <br> целеполагание, операционное управление <br> бизнесом.</p>
        </div>
        <div class="col l4 m6 s12 program__item program__item--last program__item--mid">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/project.png" alt="">
          <h3 class="program__item__title">Индивидуальная работа <br>над проектом </h3>
          <p class="program__item__text">3 месяца менторской поддержки и <br> содействия реализации проекта.</p>
        </div>
        <div class="col l4 m6 s12 program__item program__item--last program__item--ri">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/forum.png" alt="">
          <h3 class="program__item__title">Инвестиционный <br>форум</h3>
          <p class="program__item__text">Публичная защита проектов перед <br> инвесторами, представителями бизнеса и <br> СМИ.</p>
        </div>
      </div>
    </div>