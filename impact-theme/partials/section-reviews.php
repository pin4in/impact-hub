<!-- ***************** -->
<!--     REVIEWS       -->
<!-- ***************** -->
    <span id="reviews" class="anchor_nav"></span>
    <div class="container">
      <h2>Отзывы участников</h2>
      <div class="row person">
       <?php 
        // load masters data \\
        $query = new WP_Query( 'category_name=reviews' );
        if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post(); ?>
          <div class="col l6 m6 s12">
            <div class="person__item person__item--wide">
              <?php the_post_thumbnail(); ?>
              <div class="person__item__info">
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
              </div>
            </div>
          </div>
      <?php endwhile; endif; wp_reset_postdata(); ?>
      </div>
    </div>