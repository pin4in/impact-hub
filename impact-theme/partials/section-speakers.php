<!-- ***************** -->
<!--     SPEAKERS      -->
<!-- ***************** -->
    <span id="speakers" class="anchor_nav"></span>
    <div class="container">
      <h2>Преподаватели</h2>
      <div class="row person">
      <?php 
        // load masters data \\ 
        $args = array(
          'category_name'  => 'teachers',
          'order'   => 'ASC'
        );
        $query = new WP_Query( $args);
        if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post(); ?>
          <div class="col l4 m6 s12">
          <div class="person__item">
            <?php the_post_thumbnail(); ?>
            <div class="person__item__info">
              <h3><?php the_title(); ?></h3>
              <p><?php the_content(); ?></p>
            </div>
          </div>
        </div>
      <?php endwhile; endif; wp_reset_postdata(); ?>

      </div>
    </div>

<!-- ***************** -->
<!--     MASTERS       -->
<!-- ***************** -->
    
    <div class="container">
      <h2>Предприниматели</h2>
      <div class="row person">
      <?php 
        // load masters data \\
        $args = array(
          'category_name'  => 'entrepreneurs',
          'order'   => 'ASC'
        );
        $query = new WP_Query( $args);
        if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post(); ?>
          <div class="col l4 m6 s12">
          <div class="person__item">
            <?php the_post_thumbnail(); ?>
            <div class="person__item__info">
              <h3><?php the_title(); ?></h3>
              <p><?php the_content(); ?></p>
            </div>
          </div>
        </div>
      <?php endwhile; endif; wp_reset_postdata(); ?>

      </div>
    </div>
