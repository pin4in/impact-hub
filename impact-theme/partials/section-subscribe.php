<!-- ***************** -->
<!--     Subscribe     -->
<!-- ***************** -->
    <div class="container subscribe">
      <h2>Подпишитесь на рассылку Impact Hub Odessa</h2>

      <?php echo do_shortcode( '[mc4wp_form]' );?>
      
      <p class="subscribe__text">Оставьте ваш e-mail, если вы хотите получать новости о событиях и инициативах Impact HUB Odessa. <br>
      Не беспокойтесь, мы не будем заваливать вас письмами.</p>
      <div class="subscribe__separator"></div>
      <div class="subscribe__social">
          <div class="subscribe__social__item"><a href="https://plus.google.com/u/0/107572610638068706992/videos" target="_blank"></a></div>
          <div class="subscribe__social__item subscribe__social__item--tw"><a href="https://twitter.com/hub_odessa" target="_blank"></a></div>
          <div class="subscribe__social__item subscribe__social__item--fb"><a href="https://www.facebook.com/HubOdessa" target="_blank"></a></div>
      </div>
    </div>  