<!-- ***************** -->
<!--        WHO        -->
<!-- ***************** -->
    <span id="who" class="anchor_nav"></span>
    <div class="who container">
      <h2>Для кого предназначен курс</h2>
      <div class="who__line"></div>
      <p class="who__intro">Для предпринимателей нового поколения. 
Для тех, кто хочет создавать бизнес со смыслом и пользой для себя, своего общества и страны. </p>
      <div class="row">
        <div class="col l4 m6 s12 who__item">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/clock.png" alt="">
          <h3 class="who__item__title">длительность</h3>
          <div class="who__item__line"></div>
          <p class="who__item__text">2 месяца обучения (24.09 – 28.11) <br>  
+ 3 месяца индивидуальной поддержки.</p>
        </div>
        <div class="col l4 m6 s12 who__item">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/todo.png" alt="">
          <h3 class="who__item__title">график занятий</h3>
          <div class="who__item__line"></div>
          <p class="who__item__text">вторник, четверг 19.00 – 21.00; <br>
суббота 10.00 – 14.00.</p>
        </div>
        <div class="col l4 m6 s12 who__item">
          <img src="<?php echo bloginfo('template_url'); ?>/assets/img/format.png" alt="">
          <h3 class="who__item__title">формат</h3>
          <div class="who__item__line"></div>
          <p class="who__item__text">теория + практика, индивидуальная <br> работа над каждым проектом.</p>
        </div>
      </div>
    </div>