<?php /* Template Name: Registration page */ ?>

<?php get_header(); ?>
<div class="header__bg"></div>
<div class="container contacts">
	<h2>Форма регистрации</h2>
      <p>Давайте знакомится. Расскажите о себе и своем проекте</p>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
      <?php the_content(); ?>        
	
  	<?php endwhile; else : ?>
  	
  	<?php endif; ?>
    
</div>

<?php get_footer(); ?>