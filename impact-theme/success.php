<?php /* Template Name: Success */ ?>

<?php get_header(); ?>
		<div class="header__bg"></div>
		<div class="container contacts">
	      <div class="empty"></div>
	      <h2>Форма успешно отправлена!</h2>
	      <p>Мы с Вами свяжемся в ближайшее время, а пока следити за новостями в сообществах. У нас ещё много всего интересного происходит.</p>
	      <div class="subscribe__separator"></div>
	      <div class="subscribe__social">
	        <div class="subscribe__social__item"><a href="https://plus.google.com/u/0/107572610638068706992/videos"></a></div>
	        <div class="subscribe__social__item subscribe__social__item--tw"><a href="https://twitter.com/hub_odessa"></a></div>
	        <div class="subscribe__social__item subscribe__social__item--fb"><a href="https://www.facebook.com/HubOdessa"></a></div>
	      </div>
	    </div>  

		<footer class="footer--fixed">
		  <p>All contents © copyrigt 2015. Powered by Impact HUB Odessa</p>
		</footer>

        <?php wp_footer(); ?>
      
      </body>
</html>